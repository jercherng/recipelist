package com.example.jercherng.recipe;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import java.io.Serializable;
import java.util.Vector;

/**
 * Created by jerchernglaw on 2/18/16.
 */
public class CoffeeActivity extends ListActivity{
    CoffeeAdapter coffeeAdapter;
    Vector<Coffee> coffeeVector;
    ListView coffeeList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        coffeeList = getListView();

        // To retrieve object in second Activity
        CoffeeShop coffeeShop = (CoffeeShop) getIntent().getSerializableExtra("coffeeShop");

        coffeeVector = coffeeShop.getCoffeeVector();

        coffeeAdapter = new CoffeeAdapter(this, coffeeVector);
        coffeeList.setAdapter(coffeeAdapter);
    }

    @Override
    public void onListItemClick(ListView listView,
                                View itemView,
                                int position,
                                long id) {
//        A new intent is created and passed to start activity with CoffeeShopsActivity as it's context
//              and CoffeeShop as the object to be inflated into a listView

        Coffee toBeSent = coffeeVector.get((int) id);

        Intent intent = new Intent(CoffeeActivity.this, WebViewActivity.class);
        intent.putExtra(WebViewActivity.EXTRA_BLENDURL, toBeSent.getBlendUrl());
        intent.putExtra(WebViewActivity.EXTRA_BLENDRATING, toBeSent.getBlendRating());

        startActivity(intent);
    }
    public void onNextViewClick(View view){
        Intent intent = new Intent(CoffeeActivity.this, WebViewActivity.class);


        startActivity(intent);
    }

    public void onOpenWebViewClick(View view){
        Intent intent = new Intent(CoffeeActivity.this, WebViewActivity.class);

        intent.putExtra(WebViewActivity.EXTRA_BLENDURL, coffeeVector.get(1).getBlendUrl());

        startActivity(intent);
    }
//    protected void onListItemClick(ListView l, View v, int position, long id) {
//
//        Toast informUser = Toast.makeText(this, "I was clicked", Toast.LENGTH_SHORT);
//        Toast afterClicked = Toast.makeText(this, "intent code ran but not started", Toast.LENGTH_SHORT);
//
//        informUser.show();
//        // what I can do is figure out which index on the vector?
//        Coffee coffeeToBeDisplayed = new Coffee(coffeeVector.get((int) id));
//
//
//        afterClicked.show();
//    }
}
