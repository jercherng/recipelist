package com.example.jercherng.recipe;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RatingBar;
import android.widget.TextView;

import org.w3c.dom.Text;

/**
 * Created by JerCherng on 2/15/2016.
 */
public class WebViewActivity extends AppCompatActivity{

    public static final String EXTRA_BLENDURL = "blendUrl";
    public static final String EXTRA_BLENDRATING = "blendRating";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);

        // get extra from CoffeeActivity
        Intent receiveIntentFromCaller = getIntent();
        String urlToVisit = (String) receiveIntentFromCaller.getExtras().get(EXTRA_BLENDURL);
//        receiveIntentFromCaller.getExtras().get(EXTRA_BLENDRATING);

        // TODO: set rating bar rating
        RatingBar coffeeRating = (RatingBar) findViewById(R.id.coffee_web_view_rating_bar);
        coffeeRating.setRating(4);

        // TODO: webView open URL
        WebView coffeeWebView = (WebView) findViewById(R.id.coffee_web_view);
        coffeeWebView.getSettings().setJavaScriptEnabled(true);
        coffeeWebView.setWebViewClient(new MyWebViewClient());

        coffeeWebView.loadUrl(urlToVisit);
    }

    @Override
    public void finish() {

        super.finish();
    }
}
