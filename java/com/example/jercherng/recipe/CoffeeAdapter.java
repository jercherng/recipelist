package com.example.jercherng.recipe;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Vector;

/**
 * Created by jerchernglaw on 2/17/16 10:52 PM
 *  in ${PACKAGE_NAME}
 *  for Recipe.
 */

public class CoffeeAdapter extends ArrayAdapter<Coffee> {

    public CoffeeAdapter(Context context, Vector<Coffee> coffee) {
        super(context, 0, coffee);
    }

    // need to override the get view method

    @Override
    public View getView(int index, View convertView, ViewGroup parent) {
        // Get the data of Coffee on the item in this position
        final Coffee cupOfJoe = getItem(index);

        if(convertView == null) {

            // need to inflate
            LayoutInflater layoutInflater = LayoutInflater.from(getContext());

            // inflate into convertView, convertView now knows about the layout coffee_row_layout
            //  and it's widgets.
            convertView = layoutInflater.inflate(R.layout.row_layout_coffee, parent, false);
        }

        TextView blendName = (TextView) convertView.findViewById(R.id.blend_name);
        ImageView blendImage = (ImageView) convertView.findViewById(R.id.blend_image);

//        BitmapFactory.Options options = new BitmapFactory.Options();
//        options.inJustDecodeBounds = true;
//        BitmapFactory.decodeResource(getResources(), R.id.myimage, options);
//        int imageHeight = options.outHeight;
//        int imageWidth = options.outWidth;
//        String imageType = options.outMimeType;
        ImageButton deleteCoffee = (ImageButton) convertView.findViewById(R.id.delete_blend);
        final RatingBar rateCoffee = (RatingBar) convertView.findViewById(R.id.coffee_rating_bar);

        blendName.setText(cupOfJoe.getBlendName());
        blendImage.setImageResource(cupOfJoe.getBlendImageResourceId());
        blendImage.setContentDescription(cupOfJoe.getBlendName());

        rateCoffee.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                Toast.makeText(getContext(), "Rating: " + rating + " for " + cupOfJoe.getBlendName() + " set.", Toast.LENGTH_SHORT).show();
                cupOfJoe.setBlendRating((int) rating);
            }
        });

        deleteCoffee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CoffeeAdapter.this.remove(cupOfJoe);
                CoffeeAdapter.this.notifyDataSetChanged();
            }
        });

        return convertView;
    }

}
