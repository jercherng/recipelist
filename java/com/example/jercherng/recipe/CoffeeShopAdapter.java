package com.example.jercherng.recipe;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Vector;

/**
 * Created by jerchernglaw on 2/18/16.
 */

public class CoffeeShopAdapter extends ArrayAdapter<CoffeeShop> {

    public CoffeeShopAdapter(Context context, Vector<CoffeeShop> coffeeShops) {
        super(context, 0, coffeeShops);
    }

    // need to override the get view method
    @Override
    public View getView(int index, View convertView, ViewGroup parent) {
        // Get the data of Coffee on the item in this position
        // Creates a coffeeShop instance and assigns getItem(index) to
        CoffeeShop coffeeShop = getItem(index);

        if(convertView == null) {

            // need to inflate
            LayoutInflater layoutInflater = LayoutInflater.from(getContext());

            // inflate into convertView, convertView now knows about the layout coffee_row_layout
            //  and it's widgets.
            convertView = layoutInflater.inflate(R.layout.row_layout_companies, parent, false);
        }

        TextView coffeeShopName = (TextView) convertView.findViewById(R.id.coffee_shop_name);
        coffeeShopName.setText(coffeeShop.getCoffeeShopName());

        ImageView coffeeShopImage = (ImageView) convertView.findViewById(R.id.coffee_shop_image);
        coffeeShopImage.setImageResource(coffeeShop.getCompanyImageResourceId());

        return convertView;
    }
}
