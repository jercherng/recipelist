package com.example.jercherng.recipe;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import java.io.Serializable;
import java.util.Vector;

public class CoffeeShopsActivity extends ListActivity {
    private CoffeeShopAdapter companyNameListAdapter;
    private ListView companyList;
    private Vector<CoffeeShop> coffeeShops;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        coffeeShops = new Vector<>();

        // Setup the coffee here.
        Vector <Coffee> blueBottleCoffees = new Vector<>();
        Vector <Coffee> stumptownCoCoffees = new Vector<>();
        Vector <Coffee> craftCoCoffees = new Vector<>();
        Vector <Coffee> intelligensiaCoffees = new Vector<>();

        Coffee craftBedford = new Coffee("Bedford", "", R.drawable.craft_coffee_bedford);
        craftCoCoffees.add(craftBedford);

        Coffee stumpIndonesia = new Coffee("Indonesia", "", R.drawable.bluebottle_bella_donovan);
        stumptownCoCoffees.add(stumpIndonesia);
        Coffee stumpEthiopiaDuromina = new Coffee("Ethiopia Duromina", "", R.drawable.bluebottle_bella_donovan);
        stumptownCoCoffees.add(stumpEthiopiaDuromina);
        Coffee stumpEthiopiaMordecofe = new Coffee("Ethiopia Mordecofe", "", R.drawable.bluebottle_bella_donovan);
        stumptownCoCoffees.add(stumpEthiopiaMordecofe);
        Coffee stumpEthiopiaNanoChalla = new Coffee("Ethiopia Nano Challa", "", R.drawable.bluebottle_bella_donovan);
        stumptownCoCoffees.add(stumpEthiopiaNanoChalla);
        Coffee stumpGuatemalaBellaVista = new Coffee("Guatemala Bella Vista", "", R.drawable.bluebottle_bella_donovan);
        stumptownCoCoffees.add(stumpGuatemalaBellaVista);
        Coffee stumpGuatemalaFinca= new Coffee("Guatemala Finca", "", R.drawable.bluebottle_bella_donovan);
        stumptownCoCoffees.add(stumpGuatemalaFinca);
        Coffee stumpColombia = new Coffee("Colombia", "", R.drawable.bluebottle_bella_donovan);
        stumptownCoCoffees.add(stumpColombia);
        Coffee stumpPeruChirinos = new Coffee("Peru Chirinos", "", R.drawable.bluebottle_bella_donovan);
        stumptownCoCoffees.add(stumpPeruChirinos);
        Coffee stumpRwanda = new Coffee("Rwanda", "", R.drawable.bluebottle_bella_donovan);
        stumptownCoCoffees.add(stumpRwanda);
//        Coffee stumpIndonesia = new Coffee("Indonesia", "", R.drawable.stumptown_indonesia);
//        stumptownCoCoffees.add(stumpIndonesia);
//        Coffee stumpEthiopiaDuromina = new Coffee("Ethiopia Duromina", "", R.drawable.stumptown_ethiopia_duromina);
//        stumptownCoCoffees.add(stumpEthiopiaDuromina);
//        Coffee stumpEthiopiaMordecofe = new Coffee("Ethiopia Mordecofe", "", R.drawable.stumptown_ethiopia_mordecofe);
//        stumptownCoCoffees.add(stumpEthiopiaMordecofe);
//        Coffee stumpEthiopiaNanoChalla = new Coffee("Ethiopia Nano Challa", "", R.drawable.stumptown_ethiopia_nano_challa);
//        stumptownCoCoffees.add(stumpEthiopiaNanoChalla);
//        Coffee stumpGuatemalaBellaVista = new Coffee("Guatemala Bella Vista", "", R.drawable.stumptown_guatemala_bella_vista);
//        stumptownCoCoffees.add(stumpGuatemalaBellaVista);
//        Coffee stumpGuatemalaFinca= new Coffee("Guatemala Finca", "", R.drawable.stumptown_guatemala_finca);
//        stumptownCoCoffees.add(stumpGuatemalaFinca);
//        Coffee stumpColombia = new Coffee("Colombia", "", R.drawable.stumptown_columbia);
//        stumptownCoCoffees.add(stumpColombia);
//        Coffee stumpPeruChirinos = new Coffee("Peru Chirinos", "", R.drawable.stumptown_peru_chirinos);
//        stumptownCoCoffees.add(stumpPeruChirinos);
//        Coffee stumpRwanda = new Coffee("Rwanda", "", R.drawable.stumptown_rwanda);
//        stumptownCoCoffees.add(stumpRwanda);

        Coffee blueBottleBellaDonovan = new Coffee("Bella Donovan", "https://bluebottlecoffee.com/store/bella-donovan", R.drawable.bluebottle_bella_donovan);
        blueBottleCoffees.add(blueBottleBellaDonovan);
        Coffee blueBottleBetaBlend = new Coffee("Beta Blend", "https://bluebottlecoffee.com/store/beta-blend", R.drawable.bluebottle_beta_blend);
        blueBottleCoffees.add(blueBottleBetaBlend);
        Coffee blueBottleGiantSteps = new Coffee("Giant Steps", "https://bluebottlecoffee.com/store/giant-steps", R.drawable.bluebottle_giant_steps);
        blueBottleCoffees.add(blueBottleGiantSteps);
        Coffee blueBottleThreeAfricans = new Coffee("Three Africans", "https://bluebottlecoffee.com/store/three-africans", R.drawable.bluebottle_three_africans);
        blueBottleCoffees.add(blueBottleThreeAfricans);
        Coffee blueBottleTwinkle = new Coffee("Twinkle", "https://bluebottlecoffee.com/store/twinkle", R.drawable.bluebottle_twinkle);
        blueBottleCoffees.add(blueBottleTwinkle);

        Coffee intelligensiaBolivia= new Coffee("Bolivia", "http://www.intelligentsiacoffee.com/product/coffee/colonia-llusta-bolivia", R.drawable.bluebottle_twinkle);
        intelligensiaCoffees.add(intelligensiaBolivia);
        Coffee intelligensiaEros = new Coffee("Eros Blend", "http://www.intelligentsiacoffee.com/product/coffee/eros-blend", R.drawable.bluebottle_twinkle);
        intelligensiaCoffees.add(intelligensiaEros);
        Coffee intelligensiaBrazil = new Coffee("Brazil", "http://www.intelligentsiacoffee.com/product/coffee/agua-preta-brazil", R.drawable.bluebottle_twinkle);
        intelligensiaCoffees.add(intelligensiaBrazil);
        Coffee intelligensiaColombia = new Coffee("Colombia", "http://www.intelligentsiacoffee.com/product/coffee/borderlands-project-colombia", R.drawable.bluebottle_twinkle);
        intelligensiaCoffees.add(intelligensiaBolivia);
        Coffee intelligensiaInmaculadaColombia = new Coffee("Inmaculada Colombia", "http://www.intelligentsiacoffee.com/product/coffee/inmaculada-colombia-sudan-rume-special-selection", R.drawable.bluebottle_twinkle);
        intelligensiaCoffees.add(intelligensiaInmaculadaColombia);
        Coffee intelligensiaSantuarioColombia= new Coffee("Santuario Colombia", "http://www.intelligentsiacoffee.com/product/coffee/inmaculada-colombia-sudan-rume-special-selection", R.drawable.bluebottle_twinkle);
        intelligensiaCoffees.add(intelligensiaSantuarioColombia);

        // Create coffeeShops here.
        CoffeeShop blueBottleCoffee = new CoffeeShop("Blue Bottle Coffee Co.", blueBottleCoffees, R.drawable.blue_bottle_logo);
        CoffeeShop stumptownCoffee = new CoffeeShop("Stumptown Coffee Co.", stumptownCoCoffees, R.drawable.stumptown_logo);
        CoffeeShop craftCoffee = new CoffeeShop("Craft Coffee Co.", craftCoCoffees, R.drawable.craft_coffee_logo);
        CoffeeShop intelligensiaCoffee = new CoffeeShop("Intelligensia Coffee ", intelligensiaCoffees, R.drawable.intelligensia_logo);

        coffeeShops.add(blueBottleCoffee);
        coffeeShops.add(stumptownCoffee);
        coffeeShops.add(craftCoffee);
        coffeeShops.add(intelligensiaCoffee);

        companyList = getListView();

        // HERE, Drink.drinks is a static array of drinks defined inside the Drink.java class.
        companyNameListAdapter = new CoffeeShopAdapter(this, coffeeShops);
        companyList.setAdapter(companyNameListAdapter);
    }
    @Override
    public void onListItemClick(ListView listView,
                                View itemView,
                                int position,
                                long id) {
//        A new intent is created and passed to start activity with CoffeeShopsActivity as it's context
//              and CoffeeShop as the object to be inflated into a listView

        CoffeeShop specificCoffeeShop = new CoffeeShop(coffeeShops.get((int) id));

        // basically when a coffeeShop is clicked ,you want to pass all the information
        // in the coffee vector to the next activity 'CoffeeActivity' using the intent
        Intent intent = new Intent(CoffeeShopsActivity.this, CoffeeActivity.class);
        intent.putExtra("coffeeShop", (Serializable) specificCoffeeShop);

        startActivity(intent);
    }
}
