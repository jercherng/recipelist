package com.example.jercherng.recipe;

import android.content.Intent;
import android.net.Uri;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Created by JerCherng on 2/22/2016 10:19 AM
 * in com.example.jercherng.recipe
 * for Recipe.
 */
public class MyWebViewClient extends WebViewClient {
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        return false;
    }
}
