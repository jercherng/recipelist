package com.example.jercherng.recipe;

import java.io.Serializable;

/**
 * Created by jerchernglaw on 2/17/16.
 */
public class Coffee implements Serializable {
    private String blendName;
    private String blendUrl;
    private int blendImageResourceId;
    private int blendRating;

    public Coffee(Coffee copyCoffee) {
        this.blendName = copyCoffee.getBlendName();
        this.blendUrl = copyCoffee.getBlendUrl();
        this.blendImageResourceId = copyCoffee.getBlendImageResourceId();
        this.blendRating = copyCoffee.getBlendRating();
    }

    public Coffee(String blendName, String blendUrl, int blendImageResourceId) {
        this.blendName = blendName;
        this.blendUrl = blendUrl;
        this.blendImageResourceId = blendImageResourceId;
        this.blendRating = 0;
    }

    public Coffee(String blendName, String blendUrl, int blendImageResourceId, int blendRating) {
        this.blendName = blendName;
        this.blendUrl = blendUrl;
        this.blendImageResourceId = blendImageResourceId;
        this.blendRating = blendRating;
    }

    public String getBlendName() {
        return blendName;
    }

    public void setBlendName(String blendName) {
        this.blendName = blendName;
    }

    public String getBlendUrl() {
        return blendUrl;
    }

    public void setBlendUrl(String blendUrl) {
        this.blendUrl = blendUrl;
    }

    public int getBlendImageResourceId() {
        return blendImageResourceId;
    }

    public void setBlendImageResourceId(int blendImageResourceId) {
        this.blendImageResourceId = blendImageResourceId;
    }

    public int getBlendRating() {
        return blendRating;
    }

    public void setBlendRating(int blendRating) {
        this.blendRating = blendRating;
    }
}
