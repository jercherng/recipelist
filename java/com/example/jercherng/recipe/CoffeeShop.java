package com.example.jercherng.recipe;

import java.io.Serializable;
import java.util.Vector;

/**
 * Created by jerchernglaw on 2/17/16.
 */

public class CoffeeShop implements Serializable {
    private String coffeeShopName;
    private Vector<Coffee> coffeeVector;
    private int companyImageResourceId;

    public CoffeeShop() {
    }

    public CoffeeShop(CoffeeShop copyCoffeeShop) {
        this.coffeeShopName = copyCoffeeShop.getCoffeeShopName();
        this.coffeeVector = copyCoffeeShop.getCoffeeVector();
        this.companyImageResourceId = copyCoffeeShop.getCompanyImageResourceId();
    }

    public CoffeeShop(String shopName, Vector<Coffee> coffeeVector) {
        this.coffeeShopName = shopName;
        this.coffeeVector = coffeeVector;
        this.companyImageResourceId = 0;
    }

    public CoffeeShop(String shopName, Vector<Coffee> coffeeVector, int companyImageResourceId) {
        this.coffeeShopName = shopName;
        this.coffeeVector = coffeeVector;
        this.companyImageResourceId = companyImageResourceId;
    }

    public Vector<Coffee> getCoffeeVector() { return coffeeVector; }

    public void setCoffeeVector(Vector<Coffee> coffeeVector) {
        this.coffeeVector = coffeeVector;
    }

    public int getCompanyImageResourceId() {
        return companyImageResourceId;
    }

    public void setCompanyImageResourceId(int companyImageResourceId) {
        this.companyImageResourceId = companyImageResourceId;
    }

    public String getCoffeeShopName() {
        return coffeeShopName;
    }

    public void setCoffeeShopName(String coffeeShopName) {
        this.coffeeShopName = coffeeShopName;
    }
}
